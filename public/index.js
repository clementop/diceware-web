var $ = {
    new: function (node) { return document.createElement(node); },
    q: function(query) { return document.querySelectorAll(query); },
    q1: function(query) { return document.querySelector(query); },
    id: function(id) { return document.getElementById(id); },
};

var separators = {
    spaces: " ",
    dashes: "-",
    numbers: "1234567890",
    characters: "!@#$%^&*()_+|~",
};

function d6 () {
    return Math.floor(Math.random() * 6) + 1;
}

function rollDice () {
    return [
        d6(),
        d6(),
        d6(),
        d6(),
        d6(),
    ];
}

function getWord (dice) {
    return diceware.en[dice.join("")];
}

function getWords (numWords) {
    var words = [];
    var dice, word;
    for (var i = 0; i < numWords; i++) {
        dice = rollDice();
        word = getWord(dice);
        words.push([dice, word]);
    }

    return words;
}

function getSeparator (option) {
    var options = separators[option];
    if (options == null && option === "mixed") {
        options = separators.spaces + separators.numbers + separators.characters;
    }

    var index = Math.floor(Math.random() * options.length);
    return options[index];
}

function onClickGenerate () {
    var $numWords = $.id("num-words");
    var numWords = parseInt($numWords.value) || 5;

    var $betweenWords = $.id("between-words");
    var betweenWords = $betweenWords.value;

    var words = getWords(numWords);
    var phrase = "";

    for (var i = 0; i < words.length; i++) {
        var wordItem = words[i];
        var dice = wordItem[0];
        var word = wordItem[1];

        phrase += word;

        if (i < words.length - 1) {
            phrase += getSeparator(betweenWords);
        }
    }

    var $phrase = $.id("phrase");
    $phrase.value = phrase;
}

function onClickCopy () {
    var $phrase = $.id("phrase");
    $phrase.select();
    $phrase.setSelectionRange(0,99999);
    document.execCommand("copy");
}

function listen (target, event, callback) {
    target.addEventListener(event, callback);
}

function ready (fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    onClickGenerate();
    listen($.q1("button.generate"), "click", onClickGenerate);
    listen($.q1("button.copy"), "click", onClickCopy);
});
