# Diceware

Static site content for generating pass phrases using the [Diceware](https://en.wikipedia.org/wiki/Diceware) method.

Diceware wordlist sourced from the EFF's [large wordlist](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt).
